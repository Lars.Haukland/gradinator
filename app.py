from datetime import datetime
import json
import random
import re
from flask import Flask, abort, request, send_from_directory, make_response
from json import dumps
import sys
import apsw
from apsw import Error
from pygments import highlight
from pygments.lexers import SqlLexer
from pygments.formatters import HtmlFormatter
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token
from threading import local
from markupsafe import escape
from crypt import crypt
import csv
import time

# Include debug output in reply
hackit = True

# We use thread local storage for SQL, and for the pygmentize formatter (which isn't thread safe).
# (ASPW is mostly thread safe, though: https://rogerbinns.github.io/apsw/execution.html#multi-threading-and-re-entrancy
# – it should be OK to share connections, but not cursors)
tls = local()
tls.conn = None
tls.user = None

# Read database
# The database file is prepared with one fake student (negative candidate id) for each real student,
# with a corresponding plausible set of grade data. The real students have their name set to 'YOU'.
with open('resultsdb.json', 'r') as f:
    results = json.load(f)
    # map from candidateid:str → {candidateId:int, lastname:str, grade:str, scores:list[float]}
    students = results['students']
    # map from qNo:str → {qNo:int, title:str, quartiles: (float,float,float), maxPoints, min, max, median, mean, pstdev}
    questions = results['questions']
    thresholds = results['thresholds']
    thresholds_plusminus = results['thresholds_plusminus']
    # list of real candidate ids
    candIds = [student['candidateId']
               for student in students.values() if student['candidateId'] >= 0]

# Start app
app = Flask(__name__)


# For colorized SQL statements
if hackit:
    def pygmentize(text):
        if not hasattr(tls, 'formatter'):
            tls.formatter = HtmlFormatter(nowrap=True)
        if not hasattr(tls, 'lexer'):
            tls.lexer = SqlLexer()
            tls.lexer.add_filter(NameHighlightFilter(
                names=['GLOB'], tokentype=token.Keyword))
            tls.lexer.add_filter(NameHighlightFilter(
                names=['text'], tokentype=token.Name))
            tls.lexer.add_filter(KeywordCaseFilter(case='upper'))
        return f'<span class="highlight">{highlight(text, tls.lexer, tls.formatter)}</span>'

#################################################################################################

# Main entry point


@app.route('/results', methods=['GET'])
def results():
    result = {'status': 'pending'}
    try:
        candidateId = request.args.get('id', '0')
        grade = request.args.get('grade', '')
        password = request.args.get('password', '')
        if user_exists(strip_junk(candidateId), strip_junk(grade)):
            stmt = f"SELECT id, qNo, score, grade, grade_detailed, lastname FROM students NATURAL JOIN scores WHERE id = '{candidateId}' AND grade = '{grade}' AND password = '{password}' ORDER BY id, qNo"
            result['query'] = stmt
            if hackit:
                result['query_html'] = pygmentize(stmt)
            result['rows'] = []
            cursor = tls.conn.execute(stmt)
            for row in cursor:
                result['rows'].append({field[0]: value for
                                       (field, value) in zip(cursor.getdescription(), row)})
        else:
            raise Error(
                f'unknown candidate candidateId={candidateId}, grade={strip_junk(grade)}')
        result['status'] = 'ok'
        result['questions'] = questions
    except Error as e:
        result['status'] = 'error'
        result['error'] = str(e)
    resp = make_response(dumps(result))
    resp.content_type = 'application/json'
    return resp


#################################################################################################


def strip_junk(s):
    '''Remove any illegal characters at the end of the string.

    (Allows SQL injections to pass the candidate id / name check)'''

    return re.sub(r"^\s*((\w|\d|\s|-)*).*", r"\1", s).strip()


def as_int(s):
    '''Convert first digit sequence in string to int.

    (Allows SQL injections to pass the candidate id / name check)'''

    [s, *_] = re.findall(r'[0-9]+', s) or ['0']
    return int(s)


def user_exists(candidateId:str, grade:str):
    '''Check for valid user and set up database'''
    grade = grade.upper().strip()
    if grade not in ['A','B','C','D','E','F']:
        return False

    student = students.get(candidateId)
    if student == None:
        return False

    if grade != student['grade'] and not candidateId.startswith('-'):
        candidateId = '-' + candidateId
        student = students.get(candidateId)

    student = student.copy()

    if candidateId.startswith('-'):
        student['scores'] = student['scores_by_grade'][grade]
        student['grade'] = grade

    # we'll reinitialize the database if request is from a different user
    if getattr(tls, 'user', None) != candidateId+grade:
        if getattr(tls, 'conn', None) != None:
            tls.conn.close(True)
        tls.conn = None
        tls.user = None

    if getattr(tls, 'conn', None) == None:
        tls.conn = initialize_for(student)
        tls.user = candidateId+grade
    return True


def initialize_for(student):
    '''Initialize an SQLite database with data for a particular student.'''
    conn = apsw.Connection(':memory:')
    c = conn.cursor()
    c.execute('''CREATE TABLE students (
        id integer NOT NULL PRIMARY KEY, 
        lastname TEXT NOT NULL,
        password TEXT NOT NULL,
        grade CHAR(1) NOT NULL,
        grade_detailed CHAR(2) NOT NULL);''')
    c.execute('''CREATE TABLE scores (
        id integer NOT NULL, 
        qNo integer NOT NULL,
        score NUMBER NOT NULL,
        PRIMARY KEY(id, qNo));''')

    for id in candIds:
        if id == abs(student['candidateId']):
            # provide real data for the actual student
            data = student.copy()
            data['lastname'] = 'YOU'
        else:
            # user fake data for the rest (stored with negative candidate ids)
            data = students[str(-id)].copy()
            data['scores'] = data['scores_by_grade'][data['grade']]

        c.execute('insert into students values(:id, :lastname, :password, :grade, :grade_detailed)',
                  (id, data['lastname'], f'{hash(random.random()):x}', data['grade'], to_grade(data['scores'][0], True)))
        for (qNo, score) in enumerate(data['scores']):
            c.execute('insert into scores values(:id, :qNo, :score)',
                      (id, qNo, score))

    return conn

def to_grade(pts, plusminus=False):
    ths = thresholds_plusminus if plusminus else thresholds
    for (th, grade) in ths:
        if pts >= th:
            return grade
    return 'F'

#################################################################################################

# Static content – this could also be served directly by the web server


@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'favicon.png', mimetype='image/png')


@app.route('/')
@app.route('/index.html')
def index_html():
    return send_from_directory(app.root_path, 'index.html', mimetype='text/html')


#################################################################################################

# Implement minimal RFC2324 support


@app.get('/coffee/')
def nocoffee():
    abort(418)


@app.route('/coffee/', methods=['POST', 'PUT'])
def gotcoffee():
    return "Thanks!"

#################################################################################################

# CSS defs for Pygmentize. We could put this in a file somewhere and have the webserver serve
# it, but here we're just grabbing it from the library.


cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
cssModTime = datetime.now()


@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    resp.cache_control.max_age = 604800
    resp.last_modified = cssModTime
    return resp
