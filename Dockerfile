FROM python:bullseye

RUN apt-get -y update && apt-get -y install tzdata less vim sqlite3 && apt-get clean && rm -rf /var/lib/apt/lists/* && rm -f /etc/localtime && ln -s /usr/share/zoneinfo/Europe/Oslo /etc/localtime && echo Europe/Oslo > /etc/timezone
RUN pip install flask apsw pygments
RUN adduser --system tiny
USER tiny
WORKDIR /home/tiny/
COPY app.py resultsdb.json index.html favicon.* /home/tiny/
RUN (echo '#! /bin/sh' ; echo 'python -c "from app import app; app.run(host=\"0.0.0.0\")"') > runit.sh && chmod a+x runit.sh
EXPOSE 5000
CMD ["./runit.sh"]

